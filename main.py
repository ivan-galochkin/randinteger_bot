from aiogram import Bot, Dispatcher, executor, types
from aiogram.types import InlineQuery, InputTextMessageContent, InlineQueryResultArticle
import os
import hashlib
from dotenv import load_dotenv
import random

load_dotenv()

bot = Bot(token=os.getenv("token"))

dp = Dispatcher(bot)


@dp.inline_handler()
async def send_welcome(inline_query: InlineQuery):
    text = inline_query.query
    item = InlineQueryResultArticle(
        id=hashlib.md5(text.encode()).hexdigest(),
        title="None", input_message_content=InputTextMessageContent("None"))
    print(text)
    try:
        a, b = sorted(list(map(int, text.split())))
        if abs(a - b) > 10 ** 6:
            raise RangeExcess
        res = random.randint(a, b)
        item.title = f"Your random num is {res}"
        item.input_message_content = InputTextMessageContent(f"Random num in [{a}, {b}] is {res}")
        await bot.answer_inline_query(inline_query.id, results=[item], cache_time=1)
    except RangeExcess:
        item.title = "Error: range exceeded - max is 10^6"
    except ValueError as exc:
        item.title = "Error: enter two integer nums"
        item.input_message_content = InputTextMessageContent("Error")
    finally:
        await bot.answer_inline_query(inline_query.id, results=[item], cache_time=1)


class RangeExcess(BaseException):
    pass


if __name__ == '__main__':
    executor.start_polling(dp)
